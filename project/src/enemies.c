#include "common.h"

static void tick(void);
static void touch(Entity *other);
void initPlayer(void);

void initEnemies(char *line)
{
	Entity* e;

	e = malloc(sizeof(Entity));
	memset(e, 0, sizeof(Entity));
	stage.entityTail->next = e;
	stage.entityTail = e;

	sscanf(line, "%*s %f %f %f %f", &e->sx, &e->sy, &e->ex, &e->ey);

	e->health = 10;

	e->x = e->sx;
	e->y = e->sy;

	e->tick = tick;

	e->texture = loadTexture("gfx/Golem_1/PNG/PNG_Sequences/Idle/0_Golem_Idle_000.png");
	//e->texture = loadTexture("gfx/Golem_3/PNG/PNG_Sequences/Idle/0_Golem_Idle_000.png");
	SDL_QueryTexture(e->texture, NULL, NULL, &e->w, &e->h);
	e->flags = EF_SOLID + EF_WEIGHTLESS + EF_PUSH;
	e->touch = touch;
}

static void tick(void)
{
	if (fabs(self->x - self->sx) < PLATFORM_SPEED && fabs(self->y - self->sy) < PLATFORM_SPEED)
	{
		calcSlope(self->ex, self->ey, self->x, self->y, &self->dx, &self->dy);

		self->dx *= PLATFORM_SPEED;
		self->dy *= PLATFORM_SPEED;
	}

	if (fabs(self->x - self->ex) < PLATFORM_SPEED && fabs(self->y - self->ey) < PLATFORM_SPEED)
	{
		calcSlope(self->sx, self->sy, self->x, self->y, &self->dx, &self->dy);

		self->dx *= PLATFORM_SPEED;
		self->dy *= PLATFORM_SPEED;
	}
}

static void touch(Entity* other)
{
	//Entity* e;
	if (self->health > 0 && other == player)
	{

		if (app.keyboard[SDL_SCANCODE_F]) {
			self->health -= 2;
		}
		if (app.keyboard[SDL_SCANCODE_I] == 0 && !player->isOnGround){
			if (app.keyboard[SDL_SCANCODE_K]) {
				if (player->damage == 1) {
					self->health -= 1;
				}
				else if (player->damage == 10) {
					self->health -= 10;
				}
			}
		}
		if (app.keyboard[SDL_SCANCODE_S] && app.keyboard[SDL_SCANCODE_D] && player->isOnGround == 1 && player->riding == NULL) {
			if (app.keyboard[SDL_SCANCODE_F]) {
				if (player->damage == 1) {
					self->health -= 1;
				}
				else if (player->damage == 10) {
					self->health -= 10;
				}
			}
		}
		if (app.keyboard[SDL_SCANCODE_S] == 1 && app.keyboard[SDL_SCANCODE_A] == 0 && app.keyboard[SDL_SCANCODE_I] == 0 && app.keyboard[SDL_SCANCODE_D] == 0) {
			if (app.keyboard[SDL_SCANCODE_G]) {
				if (app.keyboard[SDL_SCANCODE_K]) {
					if (player->damage == 1) {
						self->health -= 5;
					}
					else if (player->damage == 10) {
						self->health -= 20;
					}
				}
			}
		}
		if (app.keyboard[SDL_SCANCODE_D] || app.keyboard[SDL_SCANCODE_A] || app.keyboard[SDL_SCANCODE_I] || app.keyboard[SDL_SCANCODE_S]) {
			player->health -= 1;
		}
	}
}
