//#ifndef STARTSCREEN_H_
//#define STARTSCREEN_H_

#include "common.h"

static void logic(void);
static void draw(void);
static void drawMenu(void);
static void drawPause(void);
static void createText(int size, char* text, SDL_Rect rect);
static SDL_Rect createMenuRect(int x, int y, int w, int h, int r, int b, int g, int a);
void check_position(int x, int y);
int map;
SDL_Rect levelsBodyOne;
SDL_Rect levelsBodyTwo;
SDL_Rect optionsBodyOne;
SDL_Rect optionsBodyTwo;

void initMenu(void){
	app.delegate.logic = logic;
	app.delegate.draw = draw;

	memset(&menu, 0, sizeof(Menu));
	draw();

	//menu.entityTail = &menu.entityHead;
}

static void logic(void)
{
	doCamera();
}

static void draw(void)
{
	SDL_Surface* tmpsurface = IMG_Load("gfx/game_background_1.png");
	SDL_Texture* background = SDL_CreateTextureFromSurface(app.renderer, tmpsurface);
	SDL_FreeSurface(tmpsurface);
	SDL_RenderCopy(app.renderer, background, NULL, NULL);
	SDL_DestroyTexture(background);
	if(pauseFlag){
		drawPause();
	}
	else{
		drawMenu();
	}
}

static void drawMenu(void){
	SDL_Rect title = createMenuRect(188, 50, 900, 45, 0, 0, 75, 255);
	createText(24, "Main Menu", title);

	SDL_Rect levelsTitle = createMenuRect(188, 95, 900, 45, 235, 235, 235, 200);
	createText(24, "Level Select", levelsTitle);

	levelsBodyOne = createMenuRect(188, 140, 450, 45, 235, 235, 235, 200);
	createText(24, "Level One", levelsBodyOne);

	levelsBodyTwo = createMenuRect(638, 140, 450, 45, 235, 235, 235, 200);
	createText(24, "Level Two", levelsBodyTwo);
}

static void drawPause(void){
	SDL_Rect title = createMenuRect(188, 50, 900, 45, 0, 0, 75, 255);
	createText(24, "PAUSED", title);

	SDL_Rect optionsTitle = createMenuRect(188, 95, 900, 45, 235, 235, 235, 200);
	createText(24, "Return to game?", optionsTitle);

	optionsBodyOne = createMenuRect(188, 140, 450, 45, 235, 235, 235, 200);
	createText(24, "Yes", optionsBodyOne);

	optionsBodyTwo = createMenuRect(638, 140, 450, 45, 235, 235, 235, 200);
	createText(24, "No", optionsBodyTwo);
}

/*int check_position(int x, int y, SDL_Rect rect){
	if(( x > rect.x ) && ( x < rect.x + rect.w ) && ( y > rect.y ) && ( y < rect.y + rect.h )){
		return 1;
	}
	else{
		return 0;
	}
}*/

//Detects which menu elements have been clicked and sets appropriate flags for game control
void check_position(int x, int y){
	//printf("RECT HAS X %d, %d, Y %d, %d\n", optionsBodyOne.x, (optionsBodyOne.x + optionsBodyOne.w), optionsBodyOne.y, (optionsBodyOne.y + optionsBodyOne.h));
	//printf("RECT HAS X %d, %d, Y %d, %d\n", optionsBodyTwo.x, (optionsBodyTwo.x + optionsBodyTwo.w), optionsBodyTwo.y, (optionsBodyTwo.y + optionsBodyTwo.h));
	if(( x > levelsBodyOne.x ) && ( x < levelsBodyOne.x + levelsBodyOne.w ) && ( y > levelsBodyOne.y ) && ( y < levelsBodyOne.y + levelsBodyOne.h )){
			map = 1;
			menuFlag = 0;
	}
	else if(( x > levelsBodyTwo.x ) && ( x < levelsBodyTwo.x + levelsBodyTwo.w ) && ( y > levelsBodyTwo.y ) && ( y < levelsBodyTwo.y + levelsBodyTwo.h )){
			map = 2;
			menuFlag = 0;
	}

	if(( x > optionsBodyOne.x ) && ( x < optionsBodyOne.x + optionsBodyOne.w ) && ( y > optionsBodyOne.y ) && ( y < optionsBodyOne.y + optionsBodyOne.h )){
			printf("RESUME\n");
			pauseFlag = 0;
			menuFlag = 0;
			gameFlag = 1;
			//runGame();
	}
	else if(( x > optionsBodyTwo.x ) && ( x < optionsBodyTwo.x + optionsBodyTwo.w ) && ( y > optionsBodyTwo.y ) && ( y < optionsBodyTwo.y + optionsBodyTwo.h )){
			printf("RETURN TO MAIN\n");
			pauseFlag = 0;
			menuFlag = 1;
			gameFlag = 0;
			printf("%d\n", gameFlag);
	}
}

//Abstracted creation of menu rectangles
static SDL_Rect createMenuRect(int x, int y, int w, int h, int r, int b, int g, int a){
	SDL_Rect rect;
	rect.x = x;
	rect.y = y;
	rect.w = w;
	rect.h = h;

	SDL_SetRenderDrawBlendMode(app.renderer, SDL_BLENDMODE_BLEND);
	SDL_SetRenderDrawColor(app.renderer, r, b, g, a);
	SDL_RenderFillRect(app.renderer, &rect);
	SDL_SetRenderDrawBlendMode(app.renderer, SDL_BLENDMODE_NONE);

	return rect;
}

//Abstracted creation of menu text elements
static void createText(int size, char* content, SDL_Rect rect){
	TTF_Font* font_type = TTF_OpenFont("gfx/Arial.ttf", size);

	if(font_type == NULL){
		printf("FONT COULD NOT BE FOUND\n");
	}

	SDL_Color color = {3, 194, 252};
	SDL_Surface* text = TTF_RenderText_Solid(font_type, content, color);
	SDL_Texture* titleText = SDL_CreateTextureFromSurface(app.renderer, text);
	SDL_QueryTexture(titleText, NULL, NULL, &rect.w, &rect.h);
	SDL_RenderCopy(app.renderer, titleText, NULL, &rect);

	TTF_CloseFont(font_type);
	SDL_DestroyTexture(titleText);
	SDL_FreeSurface(text);
}
