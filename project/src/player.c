/*
Copyright (C) 2015-2018 Parallel Realities

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

#include "common.h"
#include <time.h>
#include <unistd.h>

void initPlayer(void)
{
	player = malloc(sizeof(Entity));
	memset(player, 0, sizeof(Entity));
	stage.entityTail->next = player;
	stage.entityTail = player;

	player->health = 2;
	player->totalHealth = 2;

	player->idle[0] = loadTexture("gfx/Golem_3/PNG/PNG_Sequences/Idle/0_Golem_Idle_000.png");
	player->idle[1] = loadTexture("gfx/Golem_3/PNG/PNG_Sequences/Idle/0_Golem_Idle_000_FLIPPED.png");

	player->jump[0] = loadTexture("gfx/Golem_3/PNG/PNG_Sequences/Jump_Start/0_Golem_Jump_Start_000.png");
	player->jump[1] = loadTexture("gfx/Golem_3/PNG/PNG_Sequences/Jump_Start/0_Golem_Jump_Start_001.png");
	player->jump[2] = loadTexture("gfx/Golem_3/PNG/PNG_Sequences/Jump_Start/0_Golem_Jump_Start_002.png");
	player->jump[3] = loadTexture("gfx/Golem_3/PNG/PNG_Sequences/Jump_Start/0_Golem_Jump_Start_003.png");
	player->jump[4] = loadTexture("gfx/Golem_3/PNG/PNG_Sequences/Jump_Start/0_Golem_Jump_Start_004.png");
	player->jump[5] = loadTexture("gfx/Golem_3/PNG/PNG_Sequences/Jump_Start/0_Golem_Jump_Start_005.png");
	player->jump[6] = loadTexture("gfx/Golem_3/PNG/PNG_Sequences/Jump_Loop/0_Golem_Jump_Loop_000.png");
	player->jump[7] = loadTexture("gfx/Golem_3/PNG/PNG_Sequences/Jump_Loop/0_Golem_Jump_Loop_001.png");
	player->jump[8] = loadTexture("gfx/Golem_3/PNG/PNG_Sequences/Jump_Loop/0_Golem_Jump_Loop_002.png");
	player->jump[9] = loadTexture("gfx/Golem_3/PNG/PNG_Sequences/Jump_Loop/0_Golem_Jump_Loop_003.png");
	player->jump[10] = loadTexture("gfx/Golem_3/PNG/PNG_Sequences/Jump_Loop/0_Golem_Jump_Loop_004.png");
	player->jump[11] = loadTexture("gfx/Golem_3/PNG/PNG_Sequences/Jump_Loop/0_Golem_Jump_Loop_005.png");

	player->jump_flipped[0] = loadTexture("gfx/Golem_3/PNG/PNG_Sequences/Jump_Start/0_Golem_Jump_Start_FLIPPED_000.png");
	player->jump_flipped[1] = loadTexture("gfx/Golem_3/PNG/PNG_Sequences/Jump_Start/0_Golem_Jump_Start_FLIPPED_001.png");
	player->jump_flipped[2] = loadTexture("gfx/Golem_3/PNG/PNG_Sequences/Jump_Start/0_Golem_Jump_Start_FLIPPED_002.png");
	player->jump_flipped[3] = loadTexture("gfx/Golem_3/PNG/PNG_Sequences/Jump_Start/0_Golem_Jump_Start_FLIPPED_003.png");
	player->jump_flipped[4] = loadTexture("gfx/Golem_3/PNG/PNG_Sequences/Jump_Start/0_Golem_Jump_Start_FLIPPED_004.png");
	player->jump_flipped[5] = loadTexture("gfx/Golem_3/PNG/PNG_Sequences/Jump_Start/0_Golem_Jump_Start_FLIPPED_005.png");
	player->jump_flipped[6] = loadTexture("gfx/Golem_3/PNG/PNG_Sequences/Jump_Loop/0_Golem_Jump_Loop_FLIPPED_000.png");
	player->jump_flipped[7] = loadTexture("gfx/Golem_3/PNG/PNG_Sequences/Jump_Loop/0_Golem_Jump_Loop_FLIPPED_001.png");
	player->jump_flipped[8] = loadTexture("gfx/Golem_3/PNG/PNG_Sequences/Jump_Loop/0_Golem_Jump_Loop_FLIPPED_002.png");
	player->jump_flipped[9] = loadTexture("gfx/Golem_3/PNG/PNG_Sequences/Jump_Loop/0_Golem_Jump_Loop_FLIPPED_003.png");
	player->jump_flipped[10] = loadTexture("gfx/Golem_3/PNG/PNG_Sequences/Jump_Loop/0_Golem_Jump_Loop_FLIPPED_004.png");
	player->jump_flipped[11] = loadTexture("gfx/Golem_3/PNG/PNG_Sequences/Jump_Loop/0_Golem_Jump_Loop_FLIPPED_005.png");

	player->run[0] = loadTexture("gfx/Golem_3/PNG/PNG_Sequences/Running/0_Golem_Running_000.png");
	player->run[1] = loadTexture("gfx/Golem_3/PNG/PNG_Sequences/Running/0_Golem_Running_001.png");
	player->run[2] = loadTexture("gfx/Golem_3/PNG/PNG_Sequences/Running/0_Golem_Running_002.png");
	player->run[3] = loadTexture("gfx/Golem_3/PNG/PNG_Sequences/Running/0_Golem_Running_003.png");
	player->run[4] = loadTexture("gfx/Golem_3/PNG/PNG_Sequences/Running/0_Golem_Running_004.png");
	player->run[5] = loadTexture("gfx/Golem_3/PNG/PNG_Sequences/Running/0_Golem_Running_005.png");
	player->run[6] = loadTexture("gfx/Golem_3/PNG/PNG_Sequences/Running/0_Golem_Running_006.png");
	player->run[7] = loadTexture("gfx/Golem_3/PNG/PNG_Sequences/Running/0_Golem_Running_007.png");
	player->run[8] = loadTexture("gfx/Golem_3/PNG/PNG_Sequences/Running/0_Golem_Running_008.png");
	player->run[9] = loadTexture("gfx/Golem_3/PNG/PNG_Sequences/Running/0_Golem_Running_009.png");
	player->run[10] = loadTexture("gfx/Golem_3/PNG/PNG_Sequences/Running/0_Golem_Running_010.png");
	player->run[11] = loadTexture("gfx/Golem_3/PNG/PNG_Sequences/Running/0_Golem_Running_011.png");

	player->run_flipped[0] = loadTexture("gfx/Golem_3/PNG/PNG_Sequences/Running/0_Golem_Running_FLIPPED_000.png");
	player->run_flipped[1] = loadTexture("gfx/Golem_3/PNG/PNG_Sequences/Running/0_Golem_Running_FLIPPED_001.png");
	player->run_flipped[2] = loadTexture("gfx/Golem_3/PNG/PNG_Sequences/Running/0_Golem_Running_FLIPPED_002.png");
	player->run_flipped[3] = loadTexture("gfx/Golem_3/PNG/PNG_Sequences/Running/0_Golem_Running_FLIPPED_003.png");
	player->run_flipped[4] = loadTexture("gfx/Golem_3/PNG/PNG_Sequences/Running/0_Golem_Running_FLIPPED_004.png");
	player->run_flipped[5] = loadTexture("gfx/Golem_3/PNG/PNG_Sequences/Running/0_Golem_Running_FLIPPED_005.png");
	player->run_flipped[6] = loadTexture("gfx/Golem_3/PNG/PNG_Sequences/Running/0_Golem_Running_FLIPPED_006.png");
	player->run_flipped[7] = loadTexture("gfx/Golem_3/PNG/PNG_Sequences/Running/0_Golem_Running_FLIPPED_007.png");
	player->run_flipped[8] = loadTexture("gfx/Golem_3/PNG/PNG_Sequences/Running/0_Golem_Running_FLIPPED_008.png");
	player->run_flipped[9] = loadTexture("gfx/Golem_3/PNG/PNG_Sequences/Running/0_Golem_Running_FLIPPED_009.png");
	player->run_flipped[10] = loadTexture("gfx/Golem_3/PNG/PNG_Sequences/Running/0_Golem_Running_FLIPPED_010.png");
	player->run_flipped[11] = loadTexture("gfx/Golem_3/PNG/PNG_Sequences/Running/0_Golem_Running_FLIPPED_011.png");


	player->texture = player->idle[0];
	player->animated = 1;

	SDL_QueryTexture(player->texture, NULL, NULL, &player->w, &player->h);
}

void doPlayer(void)
{
	player->dx = 0;

	if (app.keyboard[SDL_SCANCODE_A])
	{
		player->dx = -PLAYER_MOVE_SPEED;

		//int size = sizeof(player->jump);
		//animate(player->run_flipped, size, player->x - stage.camera.x, player->y - stage.camera.y);

		if(app.keyboard[SDL_SCANCODE_I]){
			for(int i = 0; i <= 11; i++){
				player->texture = player->jump_flipped[i];
			}
		}
		else{
			for(int i = 0; i <= 11; i++){
				player->texture = player->run_flipped[i];
			}
		}
	}

	if (app.keyboard[SDL_SCANCODE_D])
	{
		player->dx = PLAYER_MOVE_SPEED;

		//int size = sizeof(player->jump);
		//animate(player->run, size, player->x - stage.camera.x, player->y - stage.camera.y);
		if(app.keyboard[SDL_SCANCODE_I]){
			for(int i = 0; i <= 11; i++){
				player->texture = player->jump[i];
			}
		}
		else{
			for(int i = 0; i <= 11; i++){
				player->texture = player->run[i];
			}
		}
	}

	if (app.keyboard[SDL_SCANCODE_I] && player->isOnGround)
	{
		player->riding = NULL;

		player->dy = -20;
		for(int i = 0; i <= 11; i++){
			player->texture = player->jump[i];
		}
		playSound(SND_JUMP, CH_PLAYER);
		//int size = sizeof(player->jump);
		//animate(player->jump, size, player->x - stage.camera.x, player->y - stage.camera.y);
		/*if(player->isInAir == 0){
			player->texture = player->idle[0];
		}*/
	}


	if (app.keyboard[SDL_SCANCODE_SPACE])
	{
		player->x = player->y = 0;

		app.keyboard[SDL_SCANCODE_SPACE] = 0;
	}

	if (app.keyboard[SDL_SCANCODE_ESCAPE]){
		pauseFlag = 1;		
	}
}
