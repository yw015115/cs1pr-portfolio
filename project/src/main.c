/*
Copyright (C) 2015-2018 Parallel Realities

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

#include "common.h"

static void capFrameRate(long *then, float *remainder);
static void runMenu(void);
void runGame(void);
//Flags allow for simple controls for running the game without having to pass arguments between multiple functions
int menuFlag = 1;
int gameFlag = 1;
int pauseFlag = 0;

int main(int argc, char *argv[]){
	memset(&app, 0, sizeof(App));
	app.textureTail = &app.textureHead;

	long then;
	float remainder;

	initSDL();

	atexit(cleanup);

	runMenu();

	initGame();

	initStage();

	then = SDL_GetTicks();

	remainder = 0;

	while(gameFlag){
		printf("LOOPING\n");
		if(pauseFlag){
			menuFlag = 1;
			runMenu();
		}
		runGame();
		capFrameRate(&then, &remainder);
	}
	printf("EXITING GAME\n");

	/*if(menuFlag){
		printf("BACK AT MENU\n");
		runMenu();
	}*/

	return 0;
}

//Refactored while loops for easier control of game state 
static void runMenu(void){
	while(menuFlag){
		prepareScene();
		doInput();

		initMenu();

		presentScene();
	}
	printf("EXITING MENU\n");
}

//Refactored while loops for easier control of game state
void runGame(void){
	printf("STARTING GAME\n");
	prepareScene();

	doInput();

	app.delegate.logic();

	app.delegate.draw();

	presentScene();
}

static void capFrameRate(long *then, float *remainder)
{
	long wait, frameTime;

	wait = 16 + *remainder;

	*remainder -= (int)*remainder;

	frameTime = SDL_GetTicks() - *then;

	wait -= frameTime;

	if (wait < 1)
	{
		wait = 1;
	}

	SDL_Delay(wait);

	*remainder += 0.667;

	*then = SDL_GetTicks();
}
