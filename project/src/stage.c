/*
Copyright (C) 2015-2018 Parallel Realities

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

#include "common.h"

static void logic(void);
static void draw(void);
static void drawHud(void);

void initStage(void)
{
	app.delegate.logic = logic;
	app.delegate.draw = draw;

	memset(&stage, 0, sizeof(Stage));

	stage.entityTail = &stage.entityHead;

	initEntities();

	initPlayer();

	initMap();
}

static void logic(void)
{
	doPlayer();

	doEntities();

	doCamera();
}

static void draw(void)
{
	SDL_Surface* tmpsurface = IMG_Load("gfx/game_background_1.png");
	SDL_Texture* background = SDL_CreateTextureFromSurface(app.renderer, tmpsurface);
	SDL_FreeSurface(tmpsurface);
	SDL_RenderCopy(app.renderer, background, NULL, NULL);
	SDL_DestroyTexture(background);
	//SDL_RenderPresent(app.renderer);

	drawMap();

	drawEntities();

	drawHud();
}

static void drawHud(void)
{
	SDL_Rect rCoins, rHealth, coinsI, healthI;

	//Coords & size of rectangle for HUD health background and text
	rHealth.x = 0;
	rHealth.y = 675;
	rHealth.w = 100;
	rHealth.h = 45;

	//Coords & size of rectangle for HUD coins background and text
	rCoins.x = 1165;
	rCoins.y = 675;
	rCoins.w = 150;
	rCoins.h = 45;

	//Coords & size of rectangle for HUD health image
	healthI.x = 0;
	healthI.y = 680;
	healthI.w = 35;
	healthI.h = 34;

	//Coords & size of rectangle for HUD coins image
	coinsI.x = 1170;
	coinsI.y = 680;
	coinsI.w = 35;
	coinsI.h = 34;

	//Draw coloured backgrounds for HUD
	SDL_SetRenderDrawBlendMode(app.renderer, SDL_BLENDMODE_BLEND);
	SDL_SetRenderDrawColor(app.renderer, 0, 0, 75, 130);
	SDL_RenderFillRect(app.renderer, &rCoins);
	SDL_RenderFillRect(app.renderer, &rHealth);
	SDL_SetRenderDrawBlendMode(app.renderer, SDL_BLENDMODE_NONE);

	//Create texture for health image
	SDL_Surface* tmpsurface = IMG_Load("gfx/items/PNG/Bonus/Bonus_2_3.png");
	SDL_Texture* health = SDL_CreateTextureFromSurface(app.renderer, tmpsurface);
	SDL_FreeSurface(tmpsurface);

	//Create texture for coin image
	SDL_Surface* tmpsurface2 = IMG_Load("gfx/items/PNG/Bonus/Bonus_2_1.png");
	SDL_Texture* coins = SDL_CreateTextureFromSurface(app.renderer, tmpsurface2);
	SDL_FreeSurface(tmpsurface2);

	//Insert texture into HUD health image rectangle
	SDL_RenderCopy(app.renderer, health, NULL, &healthI);
	//SDL_RenderPresent(app.renderer);
	SDL_DestroyTexture(health);

	//Insert texture into HUD coins image rectangle
	SDL_RenderCopy(app.renderer, coins, NULL, &coinsI);
	//SDL_RenderPresent(app.renderer);
	SDL_DestroyTexture(coins);

	//TEXT NEXT TO healthI
	drawText(SCREEN_WIDTH - 1240, 685, 3, 194, 252, TEXT_LEFT, "%d/%d", player->health, player->totalHealth);

	//TEXT NEXT TO coinsI
	drawText(SCREEN_WIDTH - 0, 685, 3, 194, 252, TEXT_RIGHT, "%d/%d", stage.pizzaFound, stage.pizzaTotal);
}
