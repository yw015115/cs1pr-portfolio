#!/bin/bash

gcc  `sdl2-config --cflags` -Wall -Wempty-body -Werror -Wstrict-prototypes -Werror=uninitialized -Warray-bounds -g -I./src -c -o build/block.o src/block.c
gcc  `sdl2-config --cflags` -Wall -Wempty-body -Werror -Wstrict-prototypes -Werror=uninitialized -Warray-bounds -g -I./src -c -o build/camera.o src/camera.c
gcc  `sdl2-config --cflags` -Wall -Wempty-body -Werror -Wstrict-prototypes -Werror=uninitialized -Warray-bounds -g -I./src -c -o build/draw.o src/draw.c
gcc  `sdl2-config --cflags` -Wall -Wempty-body -Werror -Wstrict-prototypes -Werror=uninitialized -Warray-bounds -g -I./src -c -o build/entities.o src/entities.c
gcc  `sdl2-config --cflags` -Wall -Wempty-body -Werror -Wstrict-prototypes -Werror=uninitialized -Warray-bounds -g -I./src -c -o build/init.o src/init.c
gcc  `sdl2-config --cflags` -Wall -Wempty-body -Werror -Wstrict-prototypes -Werror=uninitialized -Warray-bounds -g -I./src -c -o build/input.o src/input.c
gcc  `sdl2-config --cflags` -Wall -Wempty-body -Werror -Wstrict-prototypes -Werror=uninitialized -Warray-bounds -g -I./src -c -o build/io.o src/io.c
gcc  `sdl2-config --cflags` -Wall -Wempty-body -Werror -Wstrict-prototypes -Werror=uninitialized -Warray-bounds -g -I./src -c -o build/main.o src/main.c
gcc  `sdl2-config --cflags` -Wall -Wempty-body -Werror -Wstrict-prototypes -Werror=uninitialized -Warray-bounds -g -I./src -c -o build/map.o src/map.c
gcc  `sdl2-config --cflags` -Wall -Wempty-body -Werror -Wstrict-prototypes -Werror=uninitialized -Warray-bounds -g -I./src -c -o build/pizza.o src/pizza.c
gcc  `sdl2-config --cflags` -Wall -Wempty-body -Werror -Wstrict-prototypes -Werror=uninitialized -Warray-bounds -g -I./src -c -o build/player.o src/player.c
gcc  `sdl2-config --cflags` -Wall -Wempty-body -Werror -Wstrict-prototypes -Werror=uninitialized -Warray-bounds -g -I./src -c -o build/platform.o src/platform.c
gcc  `sdl2-config --cflags` -Wall -Wempty-body -Werror -Wstrict-prototypes -Werror=uninitialized -Warray-bounds -g -I./src -c -o build/sound.o src/sound.c
gcc  `sdl2-config --cflags` -Wall -Wempty-body -Werror -Wstrict-prototypes -Werror=uninitialized -Warray-bounds -g -I./src -c -o build/stage.o src/stage.c
gcc  `sdl2-config --cflags` -Wall -Wempty-body -Werror -Wstrict-prototypes -Werror=uninitialized -Warray-bounds -g -I./src -c -o build/textures.o src/textures.c
gcc  `sdl2-config --cflags` -Wall -Wempty-body -Werror -Wstrict-prototypes -Werror=uninitialized -Warray-bounds -g -I./src -c -o build/text.o src/text.c
gcc  `sdl2-config --cflags` -Wall -Wempty-body -Werror -Wstrict-prototypes -Werror=uninitialized -Warray-bounds -g -I./src -c -o build/util.o src/util.c
gcc  `sdl2-config --cflags` -g -I./src -c -o build/menu.o src/menu.c
gcc  `sdl2-config --cflags` -Wall -Wempty-body -Werror -Wstrict-prototypes -Werror=uninitialized -Warray-bounds -g -I./src -c -o build/enemies.o src/enemies.c

gcc -o pp06 build/block.o build/camera.o build/draw.o build/entities.o build/init.o build/input.o build/io.o build/main.o build/map.o build/pizza.o build/player.o build/platform.o build/sound.o build/stage.o build/textures.o build/text.o build/util.o build/menu.o build/enemies.o `sdl2-config --libs` -lSDL2_mixer -lSDL2_image -lSDL2_ttf -lm
