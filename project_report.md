Module Code: CS1PR16

Assignment Report Title: Programming Project

Student Number: 20015115 

Date: April 30th 2020

Actual hrs spent for the assignment: 30

Assignment evaluation (3 key points):



# Spring Programming Project Development Report

## Introduction

Projects require management in order to minimise risk and increases the odds of successfully delivering the project goal. 
Software projects, or other projects involving an intangible product, especially require a managed approach as the risks and 
people management are harder to identify and control.[1] 

The goals of this project are trifold:

1. To illustrate good code design by extending a pre existing video game, written in C with SDL2, with new complex features.
2. Support and improve the development process through the use of diligent documentation to reduce risks and improve collaboration between developers.
3. Enhance learning & understanding of software development through the application of required programming, management and documentation skills.

To illustrate the need and utility for specific documentation this report will include several types of documents required to deliver any software
development project. In this instance what will be documented is the workflow of designing, implementing and testing the new game features:

* Project Goals - In order for any project to be successful, the problem it addresses and the intended outcome must be clearly defined and understood
* Game Features - The goal of developing software that fulfills objectives is dependant on designing and creating features that deliver defined requirments
* Feature Specification - To fulfill requirements, features must be properly designed with decisions and approaches documented
* Implementation/Design Justification - To understand, maintain and update code efficiently the appropriate design and implmentation decisions must be justified and dcocumented
* Development Process Documentation - To understand the actual outcomes of the project, diligent capturing of the development workflow and how it impacted the final product is required
* Feature Testing - To ensure quality and goal delivery, the product and it's individual features require rigorous testing
* Conclusions & Reflections - Evaluations of the final outcome and the processes that lead to it enable lessons to be learnt and applied to future projects.

A specific programming style was not chosen for this project, rather efforts were focused on applying the core tenants of good code design:

* Readability - code should be visually appealing & have descriptive naming to make it easier to read.
* Clarity of Intent - documentation should not describe what the code is doing, but why to allow collaborators to understand the purpose of both the code and the way it has been written.
* Clarity of Structure - functions, classes, loops and other control structures should be clearly identified, laid out and organised to reduce the effort needed to understand the logic of various components.
* Correctness and Maintainability - avoiding duplication where necessary will make it easier to analysis & maintain code which reduces the chances of introducing bugs in later updates.
* Modularity - closely related to the last point, highly modular code is more readable, maintainable and reuseable.

While C itself is relatively lax with enforcing styles and considerations such as whitespace, the Python style guide, PEP8[2], strongly illustrates many of these tenants and influenced decisions
relating to the visual layout, structure and design of the new game features as PEP8 recommendations naturally lead to general high quality programing styles.
Furthermore, no specific considerations were identified which detracted from the merits of PEP8 for this particular code base.

## Design

In it's original state the game is a simple 2D platform game, where the player navigates an obstacle course of static and moving terrain pieces in order to collect items.
The game is considered complete once the player has collected the specified number of items. To improve the game some additional features have been designed:

* Enemies
    *  Increases the interactivity between game and player
    *  Increases the complexity & difficulty involved in playing the game
    *  Both of these traits will increase the enjoyability of playing and completing the game
    *  Will traverse the game like the player, but with the intention of preventing the player from collecting items
    *  Will reuse large parts of the existing code base, including the "entity" struct definition to define, render and control enemy entity

    C doesn't support true classes but this approach will mimic some basics of Object Orientated Programming (OOP) using structs. As such the Entity struct that will 
    be used to implement enemies can be considered a methodless class with public attributes:
    
    <img src="https://i.imgur.com/rnNjXUa.png" alt="Entity Struct Definition">

* Start Screen
    *  When executed the game will load into an initial menu, rather than immediately into gameplay
    *  Improves the game's quality of life and provides the UI infrastructure to add additional features such as level selection and game settings
    *  Requires little interaction with the rest of the code base as it appears only before gameplay
    *  Requires relatively simple code to be written from scratch

    The basic game structure uses a while loop to execute functions that control gameplay. The most simple, readable and efficient approach to add a start screen, without restructuring
    the code, is to add another while loop that handles the function calls for the new menu before the existing loop for the game logic:
    
    <img src="https://i.imgur.com/M4r2ZkM.png" alt="Start Screen Flowchart">

* Pause Menu
    *  Allows the game to be paused at the player's discretion without loss of progress
    *  Improves the game's quality of life and the player's user experience as inevitably life events will interupt play sessions
    *  Provides the opportunity to present the player with options as features are added in later updates
    *  Toggled by pressing a specified key
    *  Requires relatively simple code that can be based on the start screen code 

    <img src="https://i.imgur.com/ltFr156.png" alt="Pause Screen Flowchart">
    
    Since both menus are simplistic graphical interfaces, the core logic is similar and can be largely reused between the two. Most notably the doInput() and check_position()
    functions provide handling of mouse clicks for both without alteration. Due to the similiar layout and appearance it made sense to extend menu.c and the runMenu() function to handle the drawing
    of the pause menu contents using boolean flags to select which menu to draw. In order to enable to pause the game at will, it made sense to extend the doPlayer function in player.c to handle ESC key presses
    as this does not need to restructure and rewrite large parts of the code base.

    
## Implementation & Development

The first consideration was whether to rewrite the game in C++ as not only is this language compatible SDL2 but it enjoys a wider range of modern features, libraries and support
that make it easier to write programs. The main benefit would have been access to true classes and full OOP implementation. 

However:
*  It would have taken a considerable time and effort to convert the entire code base to C++
*  SDL2 is still a C framework that needs to be used in a C like manner, reducing the benefit of shifting to C++
*  While convenient, true classes and a full OOP are not essiential as only the basics of these are required to implement the new features, which structs
and relevant function design provide
*  Continuing to write in C allows for work on additional features to immediately begin, reducing overall development time and complexity

With these considerations in the mind, it was decided that the additional features would be implemented in C.

To implement the new features, the games original structure was left in place and adapted - the feature code was either placed in new C files with function calls added to the games
existing function call tree, or existing C files and functions were extended as appropriate. Combined with C's void function feature, this allowed for new code to be integrated into the 
code base with minimal effort and allowed for modification and control of the game's state without having to return values which would require additional complexity to manage variables appropriately.
 
 
```c 
 void initMenu(void){
	app.delegate.logic = logic;
	app.delegate.draw = draw;

	memset(&menu, 0, sizeof(Menu));
	draw();
}
```

```c
static void draw(void)
{
	SDL_Surface* tmpsurface = IMG_Load("gfx/game_background_1.png");
	SDL_Texture* background = SDL_CreateTextureFromSurface(app.renderer, tmpsurface);
	SDL_FreeSurface(tmpsurface);
	SDL_RenderCopy(app.renderer, background, NULL, NULL);
	SDL_DestroyTexture(background);
	if(pauseFlag){
		drawPause();
	}
	else{
		drawMenu();
	}
}
```

```c
static void drawMenu(void){
	SDL_Rect title = createMenuRect(188, 50, 900, 45, 0, 0, 75, 255);
	createText(24, "Main Menu", title);

	SDL_Rect levelsTitle = createMenuRect(188, 95, 900, 45, 235, 235, 235, 200);
	createText(24, "Level Select", levelsTitle);

	levelsBodyOne = createMenuRect(188, 140, 450, 45, 235, 235, 235, 200);
	createText(24, "Level One", levelsBodyOne);

	levelsBodyTwo = createMenuRect(638, 140, 450, 45, 235, 235, 235, 200);
	createText(24, "Level Two", levelsBodyTwo);
}
```

```c
static void drawPause(void){
	SDL_Rect title = createMenuRect(188, 50, 900, 45, 0, 0, 75, 255);
	createText(24, "PAUSED", title);

	SDL_Rect optionsTitle = createMenuRect(188, 95, 900, 45, 235, 235, 235, 200);
	createText(24, "Return to game?", optionsTitle);

	optionsBodyOne = createMenuRect(188, 140, 450, 45, 235, 235, 235, 200);
	createText(24, "Yes", optionsBodyOne);

	optionsBodyTwo = createMenuRect(638, 140, 450, 45, 235, 235, 235, 200);
	createText(24, "No", optionsBodyTwo);
}
```

The above code snippets are taken from menu.C, a new C file added to the codebase. It is comprised of void functions that can be easily called sequentially as required and easily
integrated into main.C:

```c
int main(int argc, char *argv[]){
	memset(&app, 0, sizeof(App));
	app.textureTail = &app.textureHead;

	long then;
	float remainder;

	initSDL();

	atexit(cleanup);

	runMenu();

	initGame();

	initStage();

	then = SDL_GetTicks();

	remainder = 0;

	while(gameFlag){
		printf("LOOPING\n");
		if(pauseFlag){
			menuFlag = 1;
			runMenu();
		}
		runGame();
		capFrameRate(&then, &remainder);
	}
	return 0;
}

static void runMenu(void){
	while(menuFlag){
		prepareScene();
		doInput();

		initMenu();

		presentScene();
	}
}

void runGame(void){
	prepareScene();

	doInput();

	app.delegate.logic();

	app.delegate.draw();

	presentScene();
}
```

In addition, main.C has had it's while loops refactored into separate functions for improved modularity, readability and reuseability. This made it easier to integrate 
the new code into the sequence of function calls that control the game. Where appropriate, the new code reuses functions and structs to accomplish its task, as this reduces the complexity
and development of the new features.

On startup the game now displays a menu that allows you to select one of two levels to play. While rudimentary, this feature works well and the apporoach taken for its implementations
allows for easy addition of more options - the SDL rectangles simply have to be added to it's layout using the new functions and the option logic provided with new mouse triggered functions called by extendeding the
check_position(x, y) function. 

Pressing the escape key during a level will now bring up a pause menu as intended. However time constraints prevented bugs from being fixed - in it's current state clicking on any button in the menu will return the game to the start menu
and then cause the game to exit. In the first level of the game, a single enemy will spawn and move back and forth in a static line - like the pause menu time constraints meant
that this feature was not fully realised and several bugs exist. Code for handling damage and negative health were also not implemented, meaning that the game crashes when the player collides with the enemy.

Due to the time constraints it is difficult to know if these last two features are not working well because of their design, implementation or purely because of bugs and their incompleteness.
Rapid iteration & testing cycles were used during development - small segments of code were added and then the game was immediately recompiled and ran to see how the changes affected execution. Print
statements were added to the code currently being tested to observe the internal logic of functions and ensure that variables, if statements and loops were working correctly and that changes in the game state 
were being propagated as required. The outcome of the test, such as print statements, compilation errors and segfaults were used to debug problems and determine the next iteration cycle.

 
## Conclusions & Reflections

Program development comprised of rapid development cycles that were loosely compliant with aspects of Agile methodologies and were subject to trial and error. Opting to leave the overall program
structure unchanged did allow for work on the new features to immediately begin but it caused other difficulties. Chiefly that the program structure obscured the logic and made learning SDL2 harder 
as chains of function calls would often span several C files and reference definitions and variables defined elsewhere. Another significant difficulty was that the program had many components which were often
required together to implement relatively simple features. 

In the future it would be more beneficial to write a significant program with a language that supports full classes and OOP (e.g. C++) as complex programs, especially games, have many components that must work together and 
classes offer organisation and interfaces that make code more readable, modular, convenient and robust to implement. This would help reduce development time per feature and make it easier to implement more complex features
and reduce the likelyhood of bugs as components could be more concisely defined with explicit, standardised interfaces enabling more convenient interactions between components.  

With more time the pause menu and enemies would be fully implemented, additional items would be added to the start screen - such as settings for screen resolution and key bindings. Different types of enemies could
be added, along with more interactable environmental objects such as power ups, launch pads and hazards.

## Code Changes

Repository - https://csgitlab.reading.ac.uk/yw015115/cs1pr-portfolio.git

## References

[1] ‘Software Engineering 10th Edition’, I.Sommerville, 2016, Pearson Education
[2] ‘PEP 8 -- Style Guide for Python Code’, G.van Rossum, B.Warsaw, N.Coghlan, https://www.python.org/dev/peps/pep-0008/, Accessed 30/4/2020, Python Software Foundation